import React from 'react';
import ReactDOM from 'react-dom';
import WorkThisTime from './workThisTime.jsx';
import './index.less';
import './fonts.less';
import './themes/dark/dark.less';

ReactDOM.render(<WorkThisTime skin="dark" />, document.getElementById('root'));
