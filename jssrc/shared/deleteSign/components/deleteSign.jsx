import React from 'react';
import PropTypes from 'prop-types';
import './deleteSign.less';

const propTypes = {
  skin: PropTypes.string,
  structure: PropTypes.string,
  expand: PropTypes.bool.isRequired,
  onBlur: PropTypes.func.isRequired,
  deleteNote: PropTypes.func.isRequired,
  noteId: PropTypes.string
}

const defaultProps = {
  skin: "",
  structure: ""
}

const DeleteSign = props => {
  return (
    <div
      className={`delete-sign ${props.skin} ${props.structure} ${props.expand ? "expand" : ""}`}
      onClick={evt => {evt.stopPropagation()}}
      onTouchEnd={evt => {evt.stopPropagation()}}
    >
      <button
        className={`delete-button ${props.skin} ${props.structure}`}
        onClick={props.expand ? props.onBlur : props.deleteNote}
        onTouchEnd={props.expand ? props.onBlur : props.deleteNote}
        data-note-id={props.noteId}
      >
        {props.expand ? "N" : "X"}
      </button>
      <button
        className={`delete-button ${props.skin} ${props.structure}`}
        onClick={props.deleteNote}
        onTouchEnd={props.deleteNote}
        data-note-id={props.noteId}
      >
        Y
      </button>
    </div>
  );
}

DeleteSign.propTypes = propTypes;
DeleteSign.defaultProps = defaultProps;

export default DeleteSign;
