import React from 'react';
import './button.less';

const Button = props => {
  return (
    <button
      className={`button ${props.skin} ${props.structure}`}
      onClick={props.onClick}
      onMouseDown={props.onMouseDown}
    >
      {props.children}
    </button>
  );
};

Button.defaultProps = {
  skin: "",
  structure: ""
}

export default Button;
