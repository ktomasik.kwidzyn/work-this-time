import React from 'react';
import PropTypes from 'prop-types';

const propTypes = {
  skin: PropTypes.string,
  structure: PropTypes.string
};

const defaultProps = {
  skin: "",
  structure: ""
};

class ExpandableMenu extends React.Component {
  constructor(props) {
    super();
    this.state = {
      expand: false
    };
  }

  expand = () => {
    this.setState({expand: true});
  }

  hide = () => {
    this.setState({expand: false});
  }

  switchHide = () => {
    this.setState(oldState => {
      return {expand: !oldState.expand}
    })
  }

  render() {
    return (
      <div className={`expandable-menu ${this.props.skin} ${this.props.structure}`}>
        <div className={`expandable-menu-header ${this.props.skin} ${this.props.structure}`}>
          <div
            className={`expandable-menu-button ${this.props.skin} ${this.props.structure}`}
            onClick={this.switchHide}
          >
            CLICK
          </div>
        </div>
        {this.props.children}
      </div>
    );
    /*
    *TODO: finish render. Style it.
    */
  }
};

ExpandableMenu.propTypes = propTypes;
ExpandableMenu.defaultProps = defaultProps;

export default ExpandableMenu;
