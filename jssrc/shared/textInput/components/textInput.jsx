import React from 'react';
import PropTypes from 'prop-types';
import "./textInput.less";

const TextInput = props => {
  return (
    <div className={`form-text-input-label ${props.skin} ${props.structure ? props.structure : ""}`}>
      <label className={`form-text-input-label ${props.skin} ${props.structure ? props.structure : ""}`}>
        {props.label}:
      </label>
      <input
        type={props.type}
        id={props.id}
        name={props.name}
        onChange={evt => props.onChange(evt)}
        className={`form-text-input ${props.skin} ${props.structure ? props.structure : ""}`}
        key={props.id}
      />
    </div>
  );
}

export default TextInput;
