import React from 'react';
import PropTypes from 'prop-types';
import './form.less';

const Form = props => {
  return (
    <form className={`form ${props.skin} ${props.structure}`} action="" onSubmit={props.onSubmit}>
      {props.children}
    </form>
  );
};

Form.defaultProps = {
  skin: "",
  structure: "",
  onSubmit: (evt)=>{evt.preventDefault()}
};

export default Form;
