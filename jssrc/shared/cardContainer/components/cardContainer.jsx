import React from 'react';
import PropTypes from 'prop-types';
import './cardContainer.css';

const propTypes ={
  skin: PropTypes.string,
  structure: PropTypes.string
}

const defaultProps = {
  skin: "white",
  structure: "normal"
}

const CardContainer = props => {
  return (
    <div className={`card ${props.skin} ${props.structure}`}>{props.children}</div>
  );
}

export default CardContainer;
