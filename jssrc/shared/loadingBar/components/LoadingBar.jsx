import React from 'react';
import PropTypes from 'prop-types';

const propTypes = {
  stateless: PropTypes.Bool,
  state: PropTypes.Number
};

const defaultProps = {
  stateless: true,
  state: PropTypes.Number
};

const LoadingBar = props => {
  if(props.stateless) {
    return (
      <div className="loading-bar stateless"><div className="loading-bar bar stateless"></div></div>
    );}
  return <div className="loading-bar"><div className="loading-bar bar" style={`transform: scaleX(${props.state})`}></div></div>;
};

export default LoadingBar;
