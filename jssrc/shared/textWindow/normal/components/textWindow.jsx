import React from 'react';
import PropTypes from 'prop-types';
import './textWindow.less';

const propTypes = {
  children: PropTypes.string
};

const defaultProps = {
  children: ""
}

class TextWindow extends React.Component {
  constructor(props) {
    super();
    this.state = {
      writtenText: "",
      timer: null
    }
  }

  asyncTimer = () => {
    if(this.state.writtenText == this.props.children){
      return;
    }
    if(this.props.children.startsWith(this.state.writtenText)) {
      this.setState({writtenText: this.props.children.substring(0, this.state.writtenText.length+1)});
      return;
    }
    this.setState({writtenText: this.props.children.substring(0, 1)});
  }

  componentDidMount(){
    let timer = setInterval(this.asyncTimer, 75);
    this.setState({timer: timer});
  }

  componentWillUnmount(){
    clearInterval(this.state.timer)
  }

  render() {
    return (
      <div className={`text-window ${this.props.skin} ${this.props.structure}`}>
        {this.state.writtenText}
      </div>
    );
  }
}

TextWindow.propTypes = propTypes;
TextWindow.defaultProps = defaultProps;

export default TextWindow;
