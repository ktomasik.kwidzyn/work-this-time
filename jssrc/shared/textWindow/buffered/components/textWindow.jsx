import React from 'react';
import PropTypes from 'prop-types';
import './textWindow.less';

const propTypes = {
  children: PropTypes.string,
  exposePushBuffer: PropTypes.func,
  rememberLines: PropTypes.number
};

const defaultProps = {
  children: "",
  exposePushBuffer: () => {},
  rememberLines: 20
}

/*
*In order to push new text to the TextWindow you need to pass "exposePushBuffer"
*func that takes a single argument. Upon mounting, this component calls it,
*with this.bufferPush as the argument. This way you can push new things to the
*buffer. Yes, it is one way communication.

*bufferPush takes three arugments:
**The text,
*
**Whether the window should start displaying the text instantly(ignoring
**previously buffered text)
*
**Whether the window should display the text instnatly(Not letter after letter)
*/

class BufferedTextWindow extends React.Component {
  constructor(props) {
    super();
    this.state = {
      currentLine: props.rememberLines-1,
      buffer: Array(props.rememberLines).fill("\n"),
      writtenBuffer: Array(props.rememberLines).fill("\n"),
      timer: null
    }
    this.divRef = React.createRef();
  }

  bufferPush = (text, interruptDisplay=false, displayInstantly=false) => {
    //// WARNING: displayInstantly is not implemented.
    //WARNING: rememberLines is not implemented.
    this.setState((prevState) => ({
      currentLine: interruptDisplay ? prevState.buffer.length : prevState.currentLine,
      buffer: [...prevState.buffer, "\n"+text]
    }));
  }

  asyncTimer = () => {
    let currentLine = this.state.currentLine;
    const buffer = this.state.buffer;
    let writtenBuffer = this.state.writtenBuffer;
    if(currentLine == this.state.buffer.length - 1){
      //If this line is the last one: Check if the line is completed;
      if(buffer[currentLine] == writtenBuffer[currentLine]){
        return;
      }
    }
    if(buffer[currentLine] == writtenBuffer[currentLine]){
      //If line is copmleted: jump to the next line.
      currentLine++;
      writtenBuffer.push([buffer[currentLine].substring(0, 1)]);
    } else {
      let currentLineText = writtenBuffer.pop();
      currentLineText = buffer[currentLine].substring(0, currentLineText.length+1)
      writtenBuffer.push(currentLineText);
    }
    this.divRef.current.scrollBy({top: 5000});
    this.setState({currentLine: currentLine, writtenBuffer: writtenBuffer});
    return;
  }

  componentDidMount(){
    let timer = setInterval(this.asyncTimer, 75);
    this.props.exposePushBuffer(this.bufferPush);
    this.setState({timer: timer});
  }

  componentWillUnmount(){
    clearInterval(this.state.timer);
  }

  render() {
    return (
      <div className={`text-window ${this.props.skin} ${this.props.structure}`} ref={this.divRef}>
        {[...this.state.writtenBuffer, "_"]}
      </div>
    );
  }
}

BufferedTextWindow.propTypes = propTypes;
BufferedTextWindow.defaultProps = defaultProps;

export default BufferedTextWindow;
