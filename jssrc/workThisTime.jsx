import React from 'react';
import LoginPage from './components/loginPage';
import MainPage from './components/mainPage';

class WorkThisTime extends React.Component {
  constructor(props) {
    super();
    this.state = {
      loggedIn: false,
      loading: true
    }
  }

  componentDidMount() {
    fetch('/api/checkLogin', {
      method: "GET",
      credentials: "same-origin"
    }).then(resp => {
      if(resp.ok) {
        this.setState({loading: false, loggedIn: true});
        return;
      }
      this.setState({loading: false, loggedIn: false});
    });
  }

  logout = () => {
    fetch('/api/logout', {
      method: "GET",
      credentials: "same-origin"
    }).then(resp => {
      if(resp.ok)
        this.setState({loggedIn: false});
    })
  }

  render() {
    if(this.state.loading) {
      return "Loading...";
    }
    if(this.state.loggedIn) {
      return <MainPage skin={this.props.skin} logout={this.logout}/>;
    }
    return <LoginPage skin={this.props.skin} fullfillLogin={() => {this.setState({loggedIn: true})}}/>;
  }
}

export default WorkThisTime;
