import React from 'react';
import CardContainer from '../../../shared/cardContainer';
import Form from '../../../shared/form';
import Button from '../../../shared/button';
import TextInput from '../../../shared/textInput';
import TextWindow from '../../../shared/textWindow/normal';
import BufferedTextWindow from '../../../shared/textWindow/buffered';
import './loginPage.less';

class LoginPage extends React.Component {
  constructor(props){
    super();
    this.state = {
      form: {
        username: "",
        password: ""
      },
      pushMessage: null,
      redirect: false,
      processing: false
    }
  }

  handleFormChange = (evt) => {
    let input = evt.target;
    let form = this.state.form;
    this.setState({form: {...form, ...{[input.name]: input.value}}})
  }

  pushMessage = (text, interruptDisplay=false, displayInstantly=false) => {
    if(this.state.pushMessage){
      this.state.pushMessage(text, interruptDisplay, displayInstantly);
      return;
    }
    console.warn('LoginPage tried pushing a message to nonexistent TextWindow.');
  }

  handleFormSubmit = (destination) => {
    if(this.state.redirect || this.state.processing) {
      this.pushMessage("Please, stand by.", true);
      this.pushMessage("We are currently processing your previous request.")
      return;
    }
    //TODO: implement second handleFormSubmit for register. It will behave differently, still requiring the user to log in.
    this.setState({processing: true});
    let myHeaders = new Headers();
    myHeaders.append('Content-Type', 'application/json');
    fetch(`/api/${destination}`, {
      method: "POST",
      body: JSON.stringify(this.state.form),
      headers: myHeaders,
      credentials: "same-origin"
    }).then(resp => {
      if(resp.ok) {
        this.pushMessage(">Welcome_", true);
        this.pushMessage("You will be logged in soon.");
        if(!this.state.redirect) {
          this.setState({redirect: true})
          setTimeout(() => {this.props.fullfillLogin()}, 6000)
        }
        //Move onto login page there. Play Glitch anim for the love of glitch anims.
        return;
      }
      this.setState({processing: false});
      if(resp.status > 499 && resp.status < 600) {
        this.pushMessage("I apologize.", true);
        this.pushMessage("It looks like something has broken down on our side.");
        this.pushMessage("You might want to check in again later.");
        this.pushMessage("If problem persists, please, inform the admin.");
        return;
      }
      if(resp.status == 400) {
        this.pushMessage("Pardon you?", true);
        this.pushMessage("How did you manage to fuck it up?");
        return;
      }
      if(resp.status == 404) {
        this.pushMessage("I'm sorry to inform you,", true);
        this.pushMessage("but the credentials you've inputed are wrong.");
      }
    });
  }

  render() {
    return (
      <div className={`login-page ${this.props.skin} ${this.props.structure}`}>
        <CardContainer skin={this.props.skin} structure="login-page-center-card">
          <h1 className={`login-page-header ${this.props.skin}`}>>Work This Time_</h1>
          <BufferedTextWindow
            skin={this.props.skin}
            structure="login-page-text-window"
            exposePushBuffer={(f) => {
              f(">Work This Time_\n");
              f(" ");
              f("A very small task-keeping application,");
              f("Authored by saito@unculturedman.com");
              f("Feel free to use it.");
              f("Unless you bloat it to death, that is.");
              this.setState({pushMessage: f});
            }}
          />
          <Form
            skin={this.props.skin}
            structure="center-form"
            submitLabel="Login"
          >
            <TextInput
              type="text"
              id="login-username"
              name="username"
              onChange={evt => this.handleFormChange(evt)}
              skin={this.props.skin}
              label="username"
            />
            <TextInput
              type="password"
              id="login-password"
              name="password"
              onChange={evt => this.handleFormChange(evt)}
              skin={this.props.skin}
              label="password"
            />
            <div>
              <Button onClick={() => {this.handleFormSubmit('login')}} skin={this.props.skin}>Login</Button>
              <Button onClick={() => {this.handleFormSubmit('register')}} skin={this.props.skin}>Register</Button>
            </div>
          </Form>
        </CardContainer>
      </div>
    );
  }
}

LoginPage.defaultProps = {
  structure: "",
  skin: ""
};

export default LoginPage;
