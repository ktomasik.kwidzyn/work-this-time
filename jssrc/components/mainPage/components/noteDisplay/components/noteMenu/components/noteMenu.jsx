import React from 'react';
import PropTypes from 'prop-types';
import './noteMenu.less';

const propTypes = {
  skin: PropTypes.string,
  structure: PropTypes.string,
  onButtonClick: PropTypes.func
};

const defaultProps = {
  skin: "",
  struture: "",
  onButtonClick: () => {}
};

const NoteMenu = props => {
  return (
    <div className={`note-display-menu ${props.skin} ${props.structure}`}>
      <button
        className="button note-display-menu-menu-button"
        onClick={props.onButtonClick}
        onTouchEnd={props.onButtonClick}
      >
        {"<="}
      </button>
    </div>
  );
};

NoteMenu.propTypes = propTypes;
NoteMenu.defaultProps = defaultProps;

export default NoteMenu;
