import React from 'react';
import PropTypes from 'prop-types';
import NoteMenu from './noteMenu';
import NoteTitle from './noteTitle';
import './noteDisplay.less';

const propTypes = {
  noteId: PropTypes.string.isRequired,
  note: PropTypes.shape({
    text: PropTypes.string,
    title: PropTypes.string
  }),
  onMenuButtonClick: PropTypes.func.isRequired,
  onTextChange: PropTypes.func.isRequired,
  onTitleChange: PropTypes.func.isRequired
};

const defaultProps = {
  structure: "",
  skin: ""
};

class NoteDisplay extends React.Component {
  constructor(props){
    super();
  }

  render() {
    return (
      <div className={`note-display ${this.props.skin} ${this.props.structure} ${this.props.mobile ? "mobile" : ""}`} >
        <NoteMenu
          skin={this.props.skin}
          structure={this.props.mobile ? "mobile" : ""}
          onButtonClick={this.props.onMenuButtonClick}
        />
        {this.props.noteId
          ? <NoteTitle
              onTitleChange={this.props.onTitleChange}
              noteId={this.props.noteId}
            >
              {this.props.note ? this.props.note.title : ""}
            </NoteTitle>
          : null
        }
        <div className="note-text">
          {this.props.noteId
            ? <textarea
                className="note-textarea"
                onChange={this.props.onTextChange}
                value={this.props.note ? this.props.note.text : ""}
                data-note-id={this.props.noteId ? this.props.noteId : ""}
              />
            : null
          }
        </div>
      </div>
    )
  }
}

/*
<textarea className={`note-textarea ${this.props.skin}`} onChange={this.props.onChange} >
{this.props.childen}
</textarea>
*/

NoteDisplay.propTypes = propTypes;
NoteDisplay.defaultProps = defaultProps;

export default NoteDisplay;
