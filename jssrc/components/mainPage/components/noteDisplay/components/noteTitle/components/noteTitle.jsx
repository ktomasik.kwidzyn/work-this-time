import React from 'react';
import PropTypes from 'prop-types';
import './noteTitle.less';

const propTypes = {
  skin: PropTypes.string,
  structure: PropTypes.string,
  children: PropTypes.string,
  noteId: PropTypes.string.isRequired,
  onTitleChange: PropTypes.func.isRequired,
}

const defaultProps = {
  skin: "",
  structure: "",
  children: ""
}

const NoteTitle = props => {
  return (
    <input
      className={`note-title ${props.skin} ${props.structure}`}
      value={props.children}
      onChange={props.onTitleChange}
      data-note-id={props.noteId}
    />
  );
}

NoteTitle.propTypes = propTypes;
NoteTitle.defaultProps = defaultProps;

export default NoteTitle;
