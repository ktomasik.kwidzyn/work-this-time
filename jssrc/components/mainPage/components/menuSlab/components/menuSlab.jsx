import React from 'react';
import PropTypes from 'prop-types';
import './menuSlab.less';

class MenuSlab extends React.Component {
  constructor(props){
    super();
    this.state = {
      expanded: false,
      delete: false,
      copiedLink: false
    }
  }

  toggleExpand = () => {
    this.setState(oldState => {
      return {
        expanded: !oldState.expanded,
        delete: false
      }
    })
  }

  cancelDelete = () => {
    this.setState({delete: false})
  }

  copySharedLink = () => {
    navigator.clipboard.writeText(this.props.noteSharedLink).then(result => {
      this.setState({copiedLink: true});
      setTimeout(() => {this.setState({copiedLink: false})}, 5000)
    });
    //Remember that navigator.clipboard can be used only in secure contexts.
  }

  delete = () => {
    if(this.state.delete){
      this.props.noteDelete();
    }
    this.setState({delete: true})
  }

  render(){
    const structure = this.state.expanded ? "expand" : "";
    return (
      <div
        className={`menu-slab ${this.props.skin} ${structure}`}
      >
        <div className={`menu-slab-title-bar ${this.props.skin} ${this.props.structure}`}>
          <div
            className={`menu-slab-title ${this.props.skin} ${this.props.structure}`}
            onClick={this.props.menuSelect}
            data-note-id={this.props.id}
          >
            {this.props.title}
          </div>
          <div
            className={`menu-slab-expand-button ${this.props.skin} ${structure}`}
            onClick={this.toggleExpand}
          >
            V
          </div>
        </div>
        <div className={`menu-slab-opts ${this.props.skin} ${this.props.structure}`}>
          <div className={`menu-slab-shared-indicator ${this.props.skin} ${this.props.structure} ${this.props.noteSharedLink ? "" : "disabled"}`}>
            {this.props.noteSharedLink
              ? <div style={{display: 'flex', justifyContent: 'space-evenly'}}>
                <span className={`disabled ${this.props.skin} ${this.props.structure}`}>NOTE SHARED</span>
                <div
                  className={`menu-slab-copy-link ${this.props.skin} ${this.props.structure}`}
                  onClick={this.copySharedLink}
                >
                  COPY LINK
                </div>
              </div>
              : "NOTE PRIVATE"
            }
          </div>
          <div className={`menu-slab-btns`}>
            {this.state.delete
              ? <div>DELETE NOTE?</div>
              : <div onClick={this.props.noteShare}>SHARE NOTE</div>
            }
            <div className={`menu-slab-btns`}>
              {this.state.delete
                ? <>
                  <div onClick={this.delete}>YES</div>
                  <div onClick={this.cancelDelete}>NO</div>
                </>
                : <div onClick={this.delete}>DELETE NOTE</div>
              }
            </div>
          </div>
        </div>
      </div>
    );
  }
}

MenuSlab.defaultProps = {
  skin: "",
  structure: "",
  title: ">Untitled Note_"
}

MenuSlab.propTypes = {
  skin: PropTypes.skin,
  structure: PropTypes.structure,
  title: PropTypes.skin,
  id: PropTypes.string.isRequired,
  menuSelect: PropTypes.func.isRequired,
  noteDelete: PropTypes.func.isRequired,
  noteShare: PropTypes.func.isRequired,
  noteSharedLink: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.oneOf([null, undefined])
  ])
}

export default MenuSlab;
