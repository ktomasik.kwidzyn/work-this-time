import React from 'react';
import Button from '../../../shared/button';
import CardContainer from '../../../shared/cardContainer';
import DeleteSign from '../../../shared/deleteSign';
import MenuSlab from './menuSlab';
import NoteDisplay from './noteDisplay';
import NoteMenu from './noteMenu';
import './mainPage.less';

class MainPage extends React.Component {
  constructor(props) {
    super();
    this.state = {
      currentMenuTouchScrollHeight: null,
      expandMenu: false,
      loading: true,
      mediaQuery: null,
      mobile: false,
      notes: [], //Notes that are current. Their state may or may not be synced to backend.
      pauseDeleteSignInputTimer: null,
      editedNoteIds: [], //Ids of edited notes prior to edits. Notes in this array await syncing with db.
      selectedNoteId: null,
      selectedNoteForDeletion: null,
      socket: null,
      syncInterval: null
    };
  }

  componentDidMount(){
    let mediaQuery = window.matchMedia('(max-width: 600px)');
    mediaQuery.addListener(this.handleMediaQueryChange);
    let socket = new WebSocket(SOCKET_URL);

    socket.addEventListener('open', (event) => {
      socket.send(JSON.stringify({
        'type': 'command',
        'body': {
          'command': 'get_all_notes'
        }
      }));
    });

    socket.addEventListener('message', (event) => {
      let message;
      message = JSON.parse(event.data);
      switch(message.type) {
        case 'update':
          this.updateEvent(message.body);
          break;
        case 'command':
          this.commandEvent(message.body);
          break;
        case 'error':
          console.warn(`WebSocket error: ${message.message}`);
          break;
      }
    });

    socket.addEventListener('close', (event) => {
      console.log("WS connection has been closed with status: ", event.code);
    });
    //Data is sent as JSON.
    /*
    {
      type: "<REQUEST_TYPE>"
      content: //Whatever the req type dictates. Probably json, may be also string
    }
    */

    let syncInterval = setInterval(this.synchronize, 2000);

    this.setState({
      socket: socket,
      mediaQuery: mediaQuery,
      mobile: mediaQuery.matches,
      loading: false,
      expandMenu: mediaQuery.matches,
      syncInterval: syncInterval
    });
  }

  //The event argument isn't actually an event. It's object parsed from JSON.
  //Basically message['content'] that is differs between message types.
  /*For commandEvent it is
  {
    command: <string>
  }
  */
  commandEvent = (event) => {
    return;
  }

  /*For updateEvent, the message['content'] is
  {
   update: <string>,
   body: Any of the following: {Object}, [List of objects]
  }
  */
  updateEvent = (event) => {
    switch (event.update) {
      case 'set_notes':
        this.setNotes(event.body);
        break;
      case 'sync':
        this.checkSynchro(event.body);
        break;
      case 'new_note':
        this.addNewNote(event.body);
        break;
      case 'delete_note':
        this.deleteNote(event.body);
        break;
      case 'share_note':
        this.shareNote(event.body);
    }
    return;
  }

  addNewNote = note => {
    this.setState(oldState => {
      let notes = {...oldState.notes,
        [note.id]: {
          'title': note.title,
          'text': note.text,
          'sharedLink': null
        }
      };
      return {
        'notes': notes,
        'selectedNoteId': note.id
      };
    });
  }

  checkSynchro = update => {
    let unsyncedIds = update.filter(note => {
      return (
        note.text != this.state.notes[note.id].text
        || note.title != this.state.notes[note.id].title
      );
    });
    this.setState(oldState => {
      let editedNoteIds = oldState.editedNoteIds.filter(noteId => !unsyncedIds.includes(noteId));
      return {
        editedNoteIds: editedNoteIds
      }
    });
    //Deep compare
  }

  deleteNote = noteId => {
    this.setState(oldState => {
      let noteKeys = Object.keys(oldState.notes).filter(key => key != noteId);
      let newNoteEditedIds = oldState.editedNoteIds.filter(editedId => editedId != noteId);
      let newNotes = oldState.notes;
      delete newNotes[noteId];
      let newSelectedNoteId = oldState.selectedNoteId == noteId ? null : oldState.selectedNoteId;
      return {
        'notes': newNotes,
        'selectedNoteId': newSelectedNoteId,
        'editedNoteIds': newNoteEditedIds,
        selectedNoteForDeletion: null
      }
    })
  }

  synchronize = () => {
    if(this.state.editedNoteIds.length > 0){
      this.state.socket.send(JSON.stringify({
        'type': 'update',
        'body': {
          'update': 'set_notes',
          'body': this.state.editedNoteIds.map(noteId => ({
            id: noteId,
            title: this.state.notes[noteId].title,
            text: this.state.notes[noteId].text,
            sharedLink: this.state.notes[noteId].sharedLink
          }))
        }
      }))
    }
  }

  setNotes = (notes) => {
    if(Array.isArray(notes)) {
      let notesObject = notes.reduce((accumulator, item, index, array) => {
        return {...accumulator,
          [item.id]: {
            text: item.note_text,
            title: item.note_title,
            sharedLink: item.shared_link
          }
        }
      }, {});
      this.setState({notes: notesObject});
    } else {
      console.warn(`set_notes call provided invalid data(Should be an array of objects of any length): ${notes}`);
      this.state.socket.send(JSON.stringify({
        'type': 'error',
        'body': `set_notes call provided invalid data(Should be an array of objects of any length): ${notes}`
      }));
    }
  }

  shareNote = (noteData) => {
    if(noteData.id && noteData.link){
      this.setState(oldState => {
        let notes = {...oldState.notes, [noteData.id]: {...oldState.notes[noteData.id], sharedLink: noteData.link}};
        return {
          'notes': notes
        }
      })
    } else {
      console.warn(`share_note call provided invalid data(should be an object with an id and link): ${noteData}`)
    }
  }

  handleMediaQueryChange = evt => {
    this.setState({mobile: evt.matches})
  }

  handleMenuAddNote = () => {
    this.state.socket.send(JSON.stringify({
      'type': 'update',
      'body': {
        'update': 'new_note'
      }
    }));
  }


  handleMenuClick = evt => {
    let expanded = this.state.expandMenu;
    this.setState({expandMenu: !expanded});
  }

  handleMenuSelectClick = evt => {
    let noteId = evt.target.dataset.noteId;
    this.setState({
      selectedNoteId: noteId,
      expandMenu: false
    })
  }

  handleMenuSelectTouch = evt => {
    if(evt.touches){
      this.setState({currentMenuTouchScrollHeight: evt.touches[0].screenY});
    }
  }

  handleMenuSelectEnd = evt => {
    let noteId = evt.target.dataset.noteId;
    if(this.state.currentMenuTouchScrollHeight === evt.changedTouches[0].screenY){
      this.setState({
        selectedNoteId: noteId,
        expandMenu: false
      })
    }
    this.setState({
      currentMenuTouchScrollHeight: null
    });
  }

  handleMenuDeleteNote = noteId => {
    this.state.socket.send(JSON.stringify({
      'type': 'update',
      'body': {
        'update': 'delete_note',
        'body': parseInt(noteId)
      }
    }))
  }

  handleMenuShareNote = noteId => {
    this.state.socket.send(JSON.stringify({
      'type': 'update',
      'body': {
        'update': 'share_note',
        'body': parseInt(noteId)
      }
    }))
  }

  handleMenuTouchRelease = evt => {
    evt.stopPropagation();
    let expanded = this.state.expandMenu;
    this.setState({expandMenu: !expanded});
  }

  handleShowMobileMenu = evt => {
    this.synchronize();
    this.setState({expandMenu: true});
  }

  handleNoteTextChange = (evt) => {
    let text = evt.target.value;
    let noteId = parseInt(evt.target.dataset['noteId']);
    this.setState(oldState => {
      let note = {...oldState.notes[noteId], text: text};
      let notes = oldState.notes;
      notes[noteId] = note;
      if(!oldState.editedNoteIds.includes(noteId)){
        let edits = [...oldState.editedNoteIds, noteId]
        return {
          editedNoteIds: edits,
          notes: notes
        };
      }
      return {
        notes: notes
      };
    });
    //NOTE: Since setState calls in React are batched, two above setState calls
    //will be treated as if they were a single one, since they touch separate values.
  }

  handleNoteTitleChange = evt => {
    let title = evt.target.value;
    let noteId = parseInt(evt.target.dataset['noteId']);
    this.setState(oldState => {
      let note = {...oldState.notes[noteId], title: title};
      let notes = oldState.notes;
      notes[noteId] = note;
      if(!oldState.editedNoteIds.includes(noteId)){
        let edits = [...oldState.editedNoteIds, noteId]
        return {
          editedNoteIds: edits,
          notes: notes
        };
      }
      return {
        notes: notes
      };
    });
  }

  componentWillUnmount(){
    clearInterval(this.state.syncInterval);
    clearTimeout(this.state.pauseDeleteSignInputTimer);
    this.synchronize();
    //this.state.socket.close(1001);
  }

  render() {
    if(this.state.loading){
      <div className={`main-page ${this.props.skin}`}>

      </div>
    }
    return (
      <div
        className={`main-page ${this.state.mobile ? "mobile" : ""} ${this.state.expandMenu ? "show-menu" : ""} ${this.props.skin}`}
      >
        <NoteMenu
          skin={this.props.skin}
          expand={this.state.expandMenu}
          addNote={this.handleMenuAddNote}
          structure={this.state.mobile ? "mobile" : null}
          logout={this.props.logout}
        >
          {Object.keys(this.state.notes).map(noteId => {
            let note = this.state.notes[noteId];
            return (
              <MenuSlab
                key={noteId}
                id={noteId}
                skin={this.props.skin}
                menuSelect={this.handleMenuSelectClick}
                onTouchStart={this.handleMenuSelectTouch}
                onTouchEnd={this.handleMenuSelectEnd}
                noteSharedLink={note.sharedLink}
                noteDelete={() => {this.handleMenuDeleteNote(noteId)}}
                noteShare={() => {this.handleMenuShareNote(noteId)}}
                title={note.title ? note.title : ">Untitled note_"}
              >
              </MenuSlab>
            );
          })}
        </NoteMenu>
        <NoteDisplay
          skin={this.props.skin}
          loading={this.state.notes ? true : false}
          onTextChange={this.handleNoteTextChange}
          onTitleChange={this.handleNoteTitleChange}
          note={this.state.selectedNoteId ? this.state.notes[this.state.selectedNoteId] : null}
          noteId={this.state.selectedNoteId}
          mobile={this.state.mobile}
          onMenuButtonClick={this.handleShowMobileMenu}
        />
      </div>
    );
  }
}

/*
        {
          this.state.loading
          ? <div className="">Loading</div>
          : <NoteDisplay skin={this.props.skin} onChange={(evt)=>{console.log(evt)}} noteId={`${this.state.selectedNoteId}`}>
	          {this.state.selectedNoteId
            ? this.state.notes[this.state.selectedNoteId]
	            ? this.state.notes[this.state.selectedNoteId].text
		          : null
	          : null
	          }
          </NoteDisplay>
        }
*/
export default MainPage;
