import React from 'react';
import PropTypes from 'prop-types';
import CardContainer from '../../../../../shared/cardContainer';

const propTypes = {
  expand: PropTypes.bool.isRequired,
  children: PropTypes.string.isRequired,
  id: PropTypes.string.isRequired,
  skin: PropTypes.string
};

const NoteCard = props => (
  <CardContainer id={props.id} skin={props.skin} structure={props.expand ? "note" : "note hidden"}>
    <textarea className="note-text"></textarea>
  </CardContainer>
);

NoteCard.propTypes = propTypes;

export default NoteCard;
