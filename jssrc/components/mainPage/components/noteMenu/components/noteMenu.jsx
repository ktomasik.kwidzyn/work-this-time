import React from 'react';
import PropTypes from 'prop-types';
import UserMenu from '../../userMenu';
import './noteMenu.less';

const propTypes = {
  addNote: PropTypes.func.isRequired,
  expand: PropTypes.bool.isRequired,
  skin: PropTypes.string,
  structure: PropTypes.string,
  logout: PropTypes.func.isRequired
};

const defaultProps = {
  skin: "",
  structure: ""
};

const NoteMenu = props => {
  return (
    <div className={`note-menu ${props.skin} ${props.structure}`}>
      <div className={`note-menu-overbar ${props.skin} ${props.structure}`}>
        <UserMenu
          skin={props.skin}
          structure={props.structure}
          logout={props.logout}
        />
        <button className={`note-menu-add-button ${props.skin} ${props.structure}`} onClick={props.addNote}>+</button>
      </div>
      {props.children}
    </div>
  );
  //TODO: move overbar onto a separate Header component
};

NoteMenu.propTypes = propTypes;
NoteMenu.defaultProps = defaultProps;

export default NoteMenu;
