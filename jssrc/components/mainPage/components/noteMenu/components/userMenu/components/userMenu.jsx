import React from 'react';
import PropTypes from 'prop-types';

const propTypes = {
  skin: PropTypes.string,
  structure: PropTypes.structure,
  expand: PropTypes.bool
}

const defaultProps = {
  skin: "",
  structure: "",
  expand: false
}

const UserMenu = props => {
  return (
    <div className={`user-menu ${props.skin} ${props.structure}`}>
      
    </div>
  );
}

export default UserMenu;
