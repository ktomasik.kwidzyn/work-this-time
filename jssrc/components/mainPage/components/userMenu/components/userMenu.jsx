import React from 'react';
import PropTypes from 'prop-types';
import './userMenu.less';

const propTypes = {
  logout: PropTypes.func.isRequired
}

const defaultProps = {

}

class UserMenu extends React.Component {
  constructor(props){
    super();
    this.state = {
      'expanded': false
    }
  }

  expand = () => {
    this.setState({expanded: true})
  }

  hide = () => {
    this.setState({expanded: false})
  }

  render() {
    return (
      <div
        className={`user-menu-container ${this.props.skin} ${this.props.structure}`}
        onBlur={this.hide}
      >
      <button
        className={`note-menu-add-button ${this.props.skin} ${this.props.structure}`}
        onClick={this.expand}
      >
        MENU
      </button>
      <div
        className={`user-menu ${this.props.skin} ${this.props.structure} ${this.state.expanded ? "show" : ""}`}
      >
        <div
          className={`user-menu-slab ${this.props.skin} ${this.props.strucure}`}
          onClick={this.props.logout}
        >
          >LOGOUT_
        </div>
      </div>
      </div>
    );
  }
}

UserMenu.propTypes = propTypes;
UserMenu.defaultProps = defaultProps;

export default UserMenu;
