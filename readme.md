# Work This Time
A.K.A. yet another evernote clone. Basically, this is a portfolio project.

This app is [heroku](https://heroku.com) hostable - hence, Procfile exists.

FEATURES:
- Stores one's notes in a db, so they can access it on any device they want.
- Autosaves your progress, so even if you lose your internet connection, you lose little to no progress(autosaves every 2s atm).

Keep in mind this is a pet project. Code inside it do not represent quality of my regular code. I do this to mess with stuff. Maybe I will make the source look representative one day.

TODO:
VERSION 1.0:
- [x] Heroku Hello World
- [x] Auth(log in/register users)
- [x] Add possibilty to edit one's notes(You can add notes now, but not edit them).
- [X] Add possibility to actually log out xD
- [X] Add possibility to delete notes(with a confirmation dialog)
- [x] Fix onTouch event firing on mobile scrolling.
- [x] Prettify Frontend
  - [x] Main page for mobiles
  - [x] Main page for dekstops
  - [x] Animate.

VERSION: 1.1:
- [ ] Modify sync, so it compares timestamps instead of whole notes.
  - [X] Prepare DB for timestamps
  - [ ] Modify note adding & editing API
- [ ] Note sharing
  - [X] Prepare DB for note sharing
  - [ ] Add API for enabing/disabling note sharing and reading shared notes
    - [X] Enabling note sharing
    - [ ] Disabling note sharing
    - [ ] Prettify the looks
    - [ ] Find a way to use navigator.clipboard in texting(probably allow to connect to websockets by localhost, not just ip)
    - [ ] Make the shared notes look good.
  - [ ] Find a smart way to get UX dun
    - [X] Maybe dun
- [ ] Store notes in browser if it loses internet connection and restore them once the connection is up again. Also, ask which version to use if note was edited on different device by that time.(Service Workers/IDBdatabase)


Other stuff:
- [ ] Fill in socket_spec.md properly.
- [ ] Unit tests for Frontend.
- [ ] Unit tests for Backend.
- [ ] Refactor entire code so it is pretty and readable.
  - [ ] Rethink components structure
  - [ ] Use some linter pls
  - [ ] Use structure to control elements size and style for their looks.

Debug note:
When you run server locally, use your local ip, not localhost/loopback as address. This is due to how websocket address is filled in in js(look into webpack.config.js to see exactly why).
