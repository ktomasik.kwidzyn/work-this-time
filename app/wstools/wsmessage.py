from flask_login import current_user
import json

WS_MESSAGE_TYPES = [
  'error',
  'command',
  'update'
]

class Handler():
  def __init__(self, db_manager):
    self.db_manager = db_manager

  def checkUser(self):
    user = current_user
    if not user.is_authenticated or not user.is_active:
      return False
    return True

  def handle_command(self, ws, command_body):
    if command_body['command'] == 'get_all_notes':
      notes = self.db_manager.get_notes()
      ws.send(json.dumps({
        'type': 'update',
        'body': {
          'update': 'set_notes',
          'body': notes
        }
      }))
    pass

  def handle_message(self, ws):
    #whenever connection starts, check if user is authenticated.
    if not self.checkUser():
      print('[LOG]: Unauthenticated user tried connecting to ws.')
      ws.send(json.dumps({
       'type': 'error',
       'message': 'User not logged in'
      }))
      ws.close()
    while not ws.closed:
      message = ws.receive()
      if message:
        #every message received, check if user is logged in.
        if not self.checkUser():
          print('[LOG]: Unauthenticated user sent a message. This is probably because their session expired.')
          ws.send(json.dumps({
            'type': 'error',
            'message': 'User not logged in'
          }))
          ws.close()
          break
        try:
          body = json.loads(message)
          if body['type'] in WS_MESSAGE_TYPES:
            if body['type'] == 'command':
              self.handle_command(ws, body['body'])
            if body['type'] == 'update':
              self.handle_update(ws, body['body'])
          else:
            ws.send(json.dumps({
              'type': 'error',
              'message': 'Received message has unsupported type'
            }))
        except ValueError:
          ws.send(json.dumps({
            'type': 'error',
            'message': 'Received message is not a JSON'
          }))

  def handle_update(self, ws, update_body):
    if update_body['update'] == 'set_notes':
      notes = update_body['body']
      query = self.db_manager.update_many_notes(notes)
      if query is None:
        pass
        #TODO: inform client that something went wrong
      else:
        ws.send(json.dumps({
          'type': 'update',
          'body': {
            'update': 'sync',
            'body': notes
          }
        }))
        #Todo: instead of notes - send only tuples of (ID, timestamp).
    if update_body['update'] == 'new_note':
      query = self.db_manager.add_empty_note();
      if query is None:
        pass
        #TODO: inform client that something went wrong
      else:
        ws.send(json.dumps({
          'type': 'update',
          'body': {
            'update': 'new_note',
            'body': {
              'id': query[0],
              'title': query[1],
              'text': query[2]
            }
          }
        }))
    if update_body['update'] == 'delete_note':
      query = self.db_manager.delete_note(update_body['body']);
      if query is None or query == 0:
        pass
      else:
        ws.send(json.dumps({
          'type': 'update',
          'body': {
            'update': 'delete_note',
            'body': update_body['body']
          }
        }))
    if update_body['update'] == 'share_note':
      query = self.db_manager.share_note(update_body['body'])
      if query is None:
        pass
      else:
        ws.send(json.dumps({
          'type': 'update',
          'body': {
            'update': 'share_note',
            'body': {
              'id': update_body['body'],
              'link': query
            }
          }
        }))
