from base64 import b64encode
from flask import Flask, abort, render_template, request, jsonify
from flask_login import LoginManager, login_required, login_user, logout_user, current_user
from flask_sockets import Sockets
from os import environ, path, urandom
import json

#local imports
from dbtools import auth
from wstools import wsmessage

app = Flask(__name__)
app.config['REMEMBER_COOKIE_DURATION'] = 31557600 #1yr
app.config['REMEMBER_COOKIE_REFRESH_EACH_REQUEST'] = True

app.secret_key = b64encode(urandom(32)).decode('utf-8')

'''
if path.isfile('./secretfile'):
  with open('./secretfile', 'r') as secret_file:
    app.secret_key = secret_file.read().strip()
else:
  with open('./secretfile', 'w') as secret_file:
    secret_key = b64encode(urandom(32)).decode('utf-8')
    secret_file.write(secret_key+"\n")
    app.secret_key = secret_key
'''
#I found generating a random file if it doesn't exists the simplest way to
#keep my secret key secret.

login_manager = LoginManager()
login_manager.init_app(app)

db_manager = auth.Database()
db_manager.init_db()

message_handler = wsmessage.Handler(db_manager)

sockets = Sockets(app)

@sockets.route('/')
def echo_socket(ws):
  message_handler.handle_message(ws)

@login_manager.user_loader
def load_user(user_id):
  return db_manager.get_user_by_id(user_id)

@app.route('/')
def main():
  return render_template('index.html')

@app.route('/api/login', methods=["POST"])
def login():
  if request.is_json:
    form = request.get_json()
    if form['username'] and form['password']:
      user = db_manager.login_user(form['username'], form['password'])
      if user:
        login_user(user, remember=True)
        return jsonify({'message': "Success!"})
    return abort(404)
  return abort(400)

@app.route('/api/register', methods=["POST"])
def register():
  if request.is_json:
    form = request.get_json()
    if form['username'] and form['password']:
      user = db_manager.register_user(form['username'], form['password'])
      if user:
        login_user(user, remember=True)
        return jsonify({'message': "Success!"})
    return abort(401)
  return abort(400)

@app.route('/api/logout', methods=["GET"])
@login_required
def logout():
  logout_user()
  return "Logout successful."

@app.route('/api/checkLogin')
@login_required
def checkLogin():
  return "All gucci!";

@app.route('/api/notes')
@login_required
def show_all_notes():
  notes = db_manager.get_notes()
  if notes is not None:
    return jsonify(notes)
  return abort(404)

@app.route('/api/notes/<note_id>')
@login_required
def show_note(note_id):
  return abort(501)

@app.route('/api/notes/new', methods=['POST'])
@login_required
def add_note():
  note = db_manager.add_empty_note()
  if note is None:
    return abort(401)
  return jsonify(note)

@app.route('/api/notes/shared/<string:shared_id>')
def get_shared_note(shared_id):
  note = db_manager.get_shared_note(shared_id)
  if note is None:
    return abort(404)
  return jsonify({'note_title': note[1], 'note_text': note[0]})

@app.route('/shared/<string:shared_id>')
def get_shared_note_html(shared_id):
  note = db_manager.get_shared_note(shared_id)
  if note is None:
    return abort(404)
  return render_template('shared.html', note_text=note[0], note_title=note[1])

if __name__ == "__main__":
    from gevent import pywsgi
    from geventwebsocket.handler import WebSocketHandler
    server = pywsgi.WSGIServer(('', int(environ.get("PORT"))), app, handler_class=WebSocketHandler)
    server.serve_forever()
