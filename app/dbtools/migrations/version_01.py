import psycopg2
from os import environ

connection = psycopg2.connect(environ['DATABASE_URL'])
cursor = connection.cursor()

cursor.execute(
'''ALTER TABLE work_this_time.notes
ADD COLUMN last_edit TIMESTAMP NOT NULL DEFAULT '1999-01-08 04:05:06',
ADD COLUMN shared_link CHAR(40) UNIQUE;
CREATE INDEX shared_link ON work_this_time.notes (shared_link);
''')

cursor.close()
