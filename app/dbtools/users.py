from flask_login.mixins import UserMixin
from hashlib import sha512

#I created this dummy class just for easier imports, when I decide to expand on it.
class User(UserMixin):
  def __init__(self, id, username, password_hash):
    self.id = id
    self.username = username
    self.password_hash = password_hash

  def password_match(self, provided_password):
    return sha512(provided_password.encode('utf-8')).hexdigest() == self.password_hash
