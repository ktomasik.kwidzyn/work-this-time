from datetime import datetime
from os import environ
from flask_login import LoginManager, current_user
from hashlib import sha512, sha1
from psycopg2 import extras
import psycopg2

from . import users as users

class Database():
  def __init__(self):
    self.dburl = environ['DATABASE_URL']
    self.connection = None
    self.isConnected = False

  def init_db(self):
    self.connect()
    schemaText = '''
    CREATE SCHEMA IF NOT EXISTS work_this_time;

    CREATE TABLE IF NOT EXISTS work_this_time.users(
      user_id SERIAL PRIMARY KEY,
      username VARCHAR(128) UNIQUE NOT NULL,
      password_hash CHAR(128) NOT NULL
    );

    CREATE TABLE IF NOT EXISTS work_this_time.notes(
      note_id BIGSERIAL PRIMARY KEY,
      note_owner INT NOT NULL,
      note_title TEXT,
      note_text TEXT,
      last_edit TIMESTAMP NOT NULL DEFAULT '1999-01-08 04:05:06',
      shared_link CHAR(40) UNIQUE,
      FOREIGN KEY (note_owner) REFERENCES work_this_time.users(user_id) ON DELETE RESTRICT
    );

    CREATE INDEX IF NOT EXISTS shared_link ON work_this_time.notes (shared_link);
    '''
    cursor = self.connection.cursor()
    cursor.execute(schemaText)
    self.disconnect()

  def connect(self):
    if not self.isConnected:
      self.connection = psycopg2.connect(self.dburl)#sslmode='require'
      self.isConnected = True

  def disconnect(self):
    if self.isConnected:
      self.connection.commit()
      self.connection.close()
      self.isConnected = False

  def get_user(self, username, connected=False):
    if not connected:
      self.connect()
    cursor = self.connection.cursor()
    cursor.execute('SELECT * FROM work_this_time.users WHERE username = %s;', (username,))
    value = cursor.fetchone();
    cursor.close()
    if not connected:
      self.disconnect()
    if value:
      return users.User(value[0], value[1], value[2])
    return None;

  def get_user_by_id(self, user_id, connected=False):
    if not connected:
      self.connect()
    cursor = self.connection.cursor()
    cursor.execute('SELECT * FROM work_this_time.users WHERE user_id = %s;', (user_id,))
    value = cursor.fetchone();
    cursor.close()
    if not connected:
      self.disconnect()
    if value:
      return users.User(value[0], value[1], value[2])
    return None;

  def login_user(self, username, password):
    self.connect()
    user = self.get_user(username, True)
    if user:
      if user.password_match(password):
        self.disconnect()
        return user
    self.disconnect()
    return None

  def register_user(self, username, password):
    self.connect()
    if not self.get_user(username, True):
      password_hash = sha512(password.encode('utf-8')).hexdigest()
      cursor = self.connection.cursor()
      cursor.execute('INSERT INTO work_this_time.users(username, password_hash) VALUES (%s, %s) RETURNING *', (username, password_hash))
      value = cursor.fetchone()
      user = users.User(value[0], value[1], value[2])
      cursor.close()
      self.disconnect()
      return user
    self.disconnect()
    return None

  def get_notes(self):
    user = current_user
    if not user.is_authenticated or not user.is_active:
      return None
    self.connect()
    cursor = self.connection.cursor()
    cursor.execute('SELECT * FROM work_this_time.notes WHERE note_owner = %s', (user.id,))
    values = cursor.fetchall()
    values = [{'id': x[0], 'note_text': x[2], 'note_title': x[3], 'last_edit': datetime.timestamp(x[4]), 'shared_link': x[5]} for x in values]
    self.disconnect()
    return values

  def get_note(self, note_id):
    user = current_user
    if not user.is_authenticated or not user.is_active:
      return None
    self.connect()
    cursor = self.connection.cursor()
    cursor.execute('SELECT * FROM work_this_time.notes WHERE note_owner = %s AND note_id = %s', (user.id, note_id))
    value = cursor.fetchone()
    self.disconnect()
    return value

  def get_shared_note(self, shared_id):
    self.connect()
    cursor = self.connection.cursor()
    cursor.execute('SELECT note_text, note_title FROM work_this_time.notes WHERE shared_link = %s', (shared_id, ))
    value = cursor.fetchone()
    self.disconnect()
    return value

  def add_empty_note(self):
    user = current_user
    if not user.is_authenticated or not user.is_active:
      return None
    self.connect()
    cursor = self.connection.cursor()
    cursor.execute('INSERT INTO work_this_time.notes(note_owner, note_title, note_text) VALUES (%s, %s, %s) RETURNING note_id, note_title, note_text', (user.id, "New note", ""))
    note = cursor.fetchone()
    self.disconnect()
    return note

  def delete_note(self, note_id):
    user = current_user
    if not user.is_authenticated or not user.is_active:
      return None
    self.connect()
    cursor = self.connection.cursor()
    cursor.execute('DELETE FROM work_this_time.notes WHERE note_owner = %s AND note_id = %s', (user.id, note_id))
    count = cursor.rowcount;
    print('[DEBUG]: Deleted note: ', count)
    self.disconnect();
    return count

  def edit_note_text(self, note_id, note_text):
    user = current_user
    if not user.is_authenticated or not user.is_active:
      return None
    self.connect()
    cursor = self.connection.cursor()
    cursor.execute('UPDATE work_this_time.notes SET note_text = %s WHERE note_id = %s AND note_owner = %s', (note_text, note_id, user.id))
    self.disconnect()
    return True

  def edit_note_title(self, note_id, note_title):
    user = current_user
    if not user.is_authenticated or not user.is_active:
      return None
    self.connect()
    cursor = self.connection.cursor()
    cursor.execute('UPDATE work_this_time.notes SET note_title = %s WHERE note_id = %s AND note_owner = %s', (note_title, note_id, user.id))
    self.disconnect()
    return True

  def edit_note(self, note_id, note_text, note_title):
    user = current_user
    if not user.is_authenticated or not user.is_active:
      return None
    self.connect()
    cursor = self.connection.cursor()
    cursor.execute('UPDATE work_this_time.notes SET note_text = %s, note_title = %s WHERE note_id = %s AND note_owner = %s', (note_text, note_title, note_id, user.id))
    self.disconnect()
    return True

  def share_note(self, note_id):
    user = current_user
    if not user.is_authenticated or not user.is_active:
      return None
    self.connect()
    cursor = self.connection.cursor()
    link = sha1(str(note_id).encode('utf-8')).hexdigest()
    cursor.execute('UPDATE work_this_time.notes SET shared_link = %s WHERE note_id = %s AND note_owner = %s RETURNING shared_link', (link, note_id, user.id))
    link = cursor.fetchone()
    self.disconnect()
    return link[0]

  def update_many_notes(self, notes):
    user = current_user
    if not user.is_authenticated or not user.is_active:
      return None
    query_notes = map(lambda note: (note['text'], note['title'], note['id'], user.id), notes)
    self.connect()
    cursor = self.connection.cursor()
    extras.execute_batch(
      cursor,
      "UPDATE work_this_time.notes SET note_text = %s, note_title = %s WHERE note_id = %s AND note_owner = %s",
      query_notes
    )
    self.disconnect()
    return True
