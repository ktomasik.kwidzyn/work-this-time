flask >= 1.0.2
flask-sockets >= 0.2.1
flask-login >= 0.4.1
psycopg2 >= 2.8.4
