const path = require('path');
const webpack = require('webpack');

let socketUrl;

switch(process.env.NODE_ENV) {
  case 'development':
    const ip = require('ip');
    let ipAddr = ip.address();
    socketUrl = "\"ws://"+ipAddr+":5000\"";
    break;
  default:
    socketUrl = "\"ws://work-this-time.herokuapp.com\"";
}

module.exports = {
  mode: 'development',
  entry: {
   index: './jssrc/index.js'
  },
  context: path.resolve(__dirname),
  plugins: [
    new webpack.DefinePlugin({
      SOCKET_URL: socketUrl
    })
  ],
  module: {
    rules: [
      { test: /\.jsx?$/, exclude: /node_modules/, loader: "babel-loader" },
      {
        test: /\.css$/i,
        use: ['style-loader', 'css-loader'],
      },
      {
        test: /\.less$/i,
        use: [
          'style-loader',
          'css-loader',
          'less-loader',
        ],
      },
    ]
  },
  output: {
    filename: '[name].js',
    path: path.resolve(__dirname, 'app/static/js')
  },
  target: 'web'
}
