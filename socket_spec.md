Messages and types:

error: indicates that smth went wrong
command: indicates that server or client is supposed to perform an operation
  -> Reads from DB
  -> Asks for a resource
update: indicates that server or client should change it's internal state
  -> Changes state in client app
  -> Updates DB
  -> Inserts into DB


Usually the thing goes like this:
Client or server calls command message and receives update message.
